var XLive = Class.create({
	initialize: function (options) {
		this.options = jQuery.extend(true, {
            // Debug?
            debug: false,
			// Connection information
    		connection: {
    			user: 'usedId', // User establishing the connection
    			password: 'test', // We should change this, but for now keep this one by default
    			url: 'ws://<host>:<port>/' // Connection url to the websocket server
    		},
            // List of users id's which are friend of the current user (to check the status)
            friends: [],
    		// Callback executed when connection is lost
    		onConnectionLost: function () {}, 
    		 // Callback used when a new chat is established with the current user
    		onOpenChat: function (chatId, otherUserId, offline) {},
    		// Callback used to handle when a new message arrives on a chat
    		onReceiveChatMessage: function (chatId, messageId, authorId, message, timestamp) {},
            // Called when all messages are loaded
            onChatLoaded: function (chatId) {},
            // user status update
            onUserStatusUpdate: function (userId, active) {}
		}, options || {});
    	this.connected = false;
    	this.chats = {};
        this.subscriptions = [];
	},

	// returns a deferred object
	connect: function () {
    	var self = this;
    	var result = jQuery.Deferred();
    	if (!this.connected) {
            this.client = Stomp.client(this.options.connection.url);
            if (!this.options.debug)
                this.client.debug = function () {};
    		this.client.connect(this.options.connection.user, this.options.connection.password, function (res) {
    			// Good connection!
    			self.connected = true;
    			self._subscribePersonalChannel();
                self.statusManager = new StatusManager(self.client, {
                    friends: self.options.friends,
                    onUserStatusUpdate: self.options.onUserStatusUpdate
                });
    			result.resolve();
    		}, function (error) {
    			// Bad connection
    			self.connected = false;
				if (result.state() == 'pending')
					result.reject();
				else 
					self.options.onConnectionLost.apply({}, []);
    		});
    	} else result.resolve();
    	return result;
    },


    isConnected: function () {
        return this.connected;
    },

    // Immediatelly disconnects (at least we should think like so! -> WRONG!)
    disconnect: function () {
        var self = this;
        this.client.disconnect(function () {
            self.connected = false;
            self.chats = {};
        });
    },

    /**
     * Returns true or false (whether the chat was created or not)
     */
    newChat: function (subject, destination) {
        var self = this;
        var result = jQuery.Deferred();
        var chatId = this._toChatId(subject, this.options.connection.user, destination);
        if (!this.chats[chatId]) {
            var message = JSON.stringify({
                type: 'NewChatRequest',
                message: {
                    subject: chatId,
                    originUserId: this.options.connection.user,
                    destinationUserId: destination,
                }
            });
            this.client.send("/queue/manager.chat", {}, message);
            result.resolveWith({}, [chatId]);
        } else result.rejectWith({}, [chatId]);

        return result;
    },

    // Send chat message
    send: function (chatId, text) {
        return this.chats[chatId].send(this.options.connection.user, text);
    },

    currentUser: function () {
        return this.options.connection.user;
    },

    _toChatId: function (subject, originUser, destinationUser) {
        return subject + "_" + originUser + "_" + destinationUser;
    },

    _subscribePersonalChannel: function () {
        var self = this;
        var subscription = this.client.subscribe("/queue/"+this.options.connection.user, function (message) {
            if (message.body) {
                self._receive(JSON.parse(message.body));
            }
        }, {
            browser: true,
            "browser-end": false
        });

        this.subscriptions.push(subscription);
    },

    _receive: function (message) {
        if (message.type == 'NewChatResponse') {
            this._newChat(message.message);
        }
    },

    _newChat: function (info) {
        var chatId = info.subject;
        this.chats[chatId] = new Channel(this.client, {
            chatId: chatId,
            onChatLoaded: this.options.onChatLoaded,
            onReceiveChatMessage: this.options.onReceiveChatMessage
        });
        this.statusManager.add(info.otherUserId);
        this.options.onOpenChat.apply({}, [chatId, info.otherUserId]);
    }
});


var Channel = Class.create({
    initialize: function (client, options) {
        this.client = client;
        this.options = jQuery.extend(true, {
            chatId: 'someId',
            onReceiveChatMessage: function (chatId, messageId, authorId, message, timestamp) {},
            onChatLoaded: function (chatId) {}
        }, options || {});

        this.messages = {};
        this.messageList = [];
        this.subscribe();
    },

    subscribe: function () {
        var self = this;
        var subscription = this.client.subscribe("/queue/"+this.options.chatId, function (message) {
            if (message.body) {
                self._receive(JSON.parse(message.body));
            } else {
                subscription.unsubscribe();
                self.options.onChatLoaded.apply({}, [self.options.chatId]);
                self.client.subscribe("/queue/"+self.options.chatId, function (message) {
                    self._receive(JSON.parse(message.body));
                }, {
                    browser: true,
                    "browser-end": false,
                    "from-seq": -1
                });
            }
        }, {
            browser: true
        });
    },

    _receive: function (message) {
        if (!this.messages[message.id]) {
            if (message.type == 'text') {
                this.messages[message.id] = message;
                this.options.onReceiveChatMessage.apply({}, 
                    [ this.options.chatId, message.id, message.authorId, jQuery.base64.decode(message.text), message.timestamp ]
                );   
            }
        }
    },

    send: function (userId, text) {
        var result = jQuery.Deferred();
        var timestamp = new Date().getTime();
        var messageId = this.options.chatId + "_" + userId + "_" + timestamp;
        this.options.onReceiveChatMessage.apply({}, 
            [ this.options.chatId, messageId, userId, text, timestamp ]
        );
        var message = {
                type: 'text',
                id: messageId,
                authorId: userId,
                timestamp: timestamp,
                text: jQuery.base64.encode(text)
        };
        this.messages[messageId] = message;
        if (this.client.connected) {
            this.client.send("/queue/"+this.options.chatId, { persist: true }, JSON.stringify(message));    
            result.resolveWith({ id: messageId });
        } else {
            result.rejectWith({ id: messageId, code: 'connection.down' });
        }
        return result;
    }

});

var StatusManager = Class.create({
    initialize: function (client, options) {
        this.client = client;
        this.options = jQuery.extend(true, {
            friends: [],
            onUserStatusUpdate: function (userId, active) {}
        }, options || {});

        this.userIds = {};

        this.friends = [];
        for (var i=0;i<this.options.friends.length;i++)
            this.add(this.options.friends[i]);
    },

    add: function (userId) {
        if (!this.userIds[userId]) {
            this.userIds[userId] = true;
            this.friends.push(new UserStatus(this.client, {
                userId: userId,
                onUserStatusUpdate: this.options.onUserStatusUpdate
            }));
        }
    }
});

var UserStatus = Class.create({
    initialize: function (client, options) {
        this.client = client;
        this.options = jQuery.extend(true, {
            userId: 'userId',
            onUserStatusUpdate: function (userId, active) {}
        }, options || {});

        this.subscribe();
    },

    subscribe: function () {
        var self = this;
        var lastStatusIsActive = false;
        var sub = this.client.subscribe("/queue/"+this.options.userId+"_status", function (message) {
            if (message.body) {
                var body = JSON.parse(message.body);
                if (body.type == 'UserStatusUpdate') {
                    lastStatusIsActive = body.message.active;
                }
            } else {
                self.options.onUserStatusUpdate.apply({}, [self.options.userId, lastStatusIsActive]);
                sub.unsubscribe();
                self.client.subscribe("/queue/"+self.options.userId+"_status", function (message) {
                    self._receive(JSON.parse(message.body));
                }, { browser: true, "browser-end": false, "from-seq": -1 });
            }
        }, { browser: true, "browser-end": true });
    },

    _receive: function (message) {
        if (message.type == 'UserStatusUpdate') {
            this.options.onUserStatusUpdate.apply({}, [this.options.userId, message.message.active]);
        }
    }
});